
Este repositório armazena o código fonte e bases (temporarárias ou não) resultantes do trabalho que desenvolvo acerca dos discursos de ódio em textos de Língua Portuguesa. Ele contém scripts para download de comentários e de páginas no facebook, twitter e diversas páginas de notícias ou que contenham comentários candidados a possuírem discriminção de qualquer tipo.


### A seguir, uma breve descrição dos principais itens do repositório: ### 

* Diretório "face": Contém os comentários extraídos das páginas presentes em "tags-facebook.txt"	
* Diretório "twitter": Contém os twittes extraídos com base nas tags presentes em "tags-twitter.csv" separados pelas prováveis categorias de discriminação

* Diretório "done": Contém os nomes das páginas do face cujos comentários foram todos recuperados. Cada nome de página corresponde a um arquivo.

* Diretório "web": Contém os comentários extraídos de cada página de notícias.