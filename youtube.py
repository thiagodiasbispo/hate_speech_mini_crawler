#!/usr/bin/env python

from __future__ import print_function

import os
import sys
import time
import json
import requests
import argparse
import lxml.html

from lxml.cssselect import CSSSelector

import util
from db import DB

YOUTUBE_COMMENTS_URL = 'https://www.youtube.com/all_comments?v={youtube_id}'
YOUTUBE_COMMENTS_AJAX_URL = 'https://www.youtube.com/comment_ajax'

USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36'


def find_value(html, key, num_chars=2):
    pos_begin = html.find(key) + len(key) + num_chars
    pos_end = html.find('"', pos_begin)
    return html[pos_begin: pos_end]


def extract_comments(html):
    tree = lxml.html.fromstring(html)
    item_sel = CSSSelector('.comment-item')
    text_sel = CSSSelector('.comment-text-content')
    time_sel = CSSSelector('.time')
    author_sel = CSSSelector('.user-name')

    for item in item_sel(tree):
        yield {
            'social_media_object_id': item.get('data-cid'),
            'text': text_sel(item)[0].text_content(),
            'more_information': json.dumps({"create_date": time_sel(item)[0].text_content().strip()}),
            'social_user_details': json.dumps({"user_name": author_sel(item)[0].text_content()})
        }


def extract_reply_cids(html):
    tree = lxml.html.fromstring(html)
    sel = CSSSelector('.comment-replies-header > .load-comments')
    return [i.get('data-cid') for i in sel(tree)]


def ajax_request(session, url, params, data, retries=10, sleep=20):
    for _ in range(retries):
        response = session.post(url, params=params, data=data)
        if response.status_code == 200:
            response_dict = json.loads(response.text)
            return response_dict.get('page_token', None), response_dict['html_content']
        else:
            time.sleep(sleep)


def download_comments(youtube_id, sleep=1):
    session = requests.Session()
    session.headers['User-Agent'] = USER_AGENT

    # Get Youtube page with initial comments
    response = session.get(YOUTUBE_COMMENTS_URL.format(youtube_id=youtube_id))
    html = response.text
    reply_cids = extract_reply_cids(html)

    ret_cids = []
    for comment in extract_comments(html):
        ret_cids.append(comment['cid'])
        yield comment

    page_token = find_value(html, 'data-token')
    session_token = find_value(html, 'XSRF_TOKEN', 4)

    first_iteration = True

    # Get remaining comments (the same as pressing the 'Show more' button)
    while page_token:
        data = {'video_id': youtube_id,
                'session_token': session_token}

        params = {'action_load_comments': 1,
                  'order_by_time': True,
                  'filter': youtube_id}

        if first_iteration:
            params['order_menu'] = True
        else:
            data['page_token'] = page_token

        response = ajax_request(session, YOUTUBE_COMMENTS_AJAX_URL, params, data)
        if not response:
            break

        page_token, html = response

        reply_cids += extract_reply_cids(html)
        for comment in extract_comments(html):
            if comment['social_media_object_id'] not in ret_cids:
                ret_cids.append(comment['social_media_object_id'])
                yield comment

        first_iteration = False
        time.sleep(sleep)

    # Get replies (the same as pressing the 'View all X replies' link)
    for cid in reply_cids:
        data = {'comment_id': cid,
                'video_id': youtube_id,
                'can_reply': 1,
                'session_token': session_token}

        params = {'action_load_replies': 1,
                  'order_by_time': True,
                  'filter': youtube_id,
                  'tab': 'inbox'}

        response = ajax_request(session, YOUTUBE_COMMENTS_AJAX_URL, params, data)
        if not response:
            break

        _, html = response

        for comment in extract_comments(html):
            if comment['social_media_object_id'] not in ret_cids:
                ret_cids.append(comment['social_media_object_id'])
                yield comment
        time.sleep(sleep)


def get_all_videos():
    with open("web-content/youtube/politica.txt") as f:
        for video in f:
            video = video.strip().replace("\n", "")
            yield video


def get_video_id(video_url):
    return video_url.split("v=")[1]


def main():
    experiment_id = util.create_experimento_id()
    print("Experimento: ", experiment_id)
    db = DB()
    try:
        videos = set(get_all_videos()) - set(db.get_all_processed_pages_of_social_media('y'))
        for video in videos:
            youtube_id = get_video_id(video)
            comments = []
            for comment in download_comments(youtube_id):
                comment["experiment_id"] = experiment_id
                comment["hate_speech_type"] = "varios"
                comment["social_media"] = "y"
                comments.append(comment)
            db.save_all_web_page_comments(comments, video, "y", experiment_id)
            print('Done:', video)
    except Exception as e:
        print('Error:', str(e))
        sys.exit(1)


if __name__ == "__main__":
    main()
