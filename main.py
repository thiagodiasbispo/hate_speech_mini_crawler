import string

import db
import util
from os.path import join


def main():
    data_base = db.DB()
    for row in util.read_tag_like_csv_file("files/tags-twitter.csv", ","):
        tags = [tag.strip() for tag in row[1].split("/")]
        for collected_text in data_base.get_all_collected_texts_that_match(tags):
            print(collected_text.text)

def main_id():
    data_base = db.DB()
    for row in util.read_tag_like_csv_file("files/tags-twitter.csv", ","):
        tags = [tag.strip() for tag in row[1].split("/")]
        ids = tuple(str(collected_text.id) for collected_text in data_base.get_all_collected_texts_that_match(tags))
        print(row[0])
        with open(join("temp", row[0]), "w") as f:
            f.write("select text from collected_texts where id in ({})".format(",".join(ids)))

def main_count():
    data_base = db.DB()
    cont = 0
    for row in util.read_tag_like_csv_file("files/tags-twitter.csv", ","):
        tags = [tag.strip() for tag in row[1].split("/")]
        total = data_base.count_all_collected_texts_that_match(tags)
        cont += total
        print(row[0], total)
    print(cont)


def main_first_clean():
    data_base = db.DB()
    transtab = str.maketrans(string.punctuation," "*len(string.punctuation))
    for text in data_base.get_all_collected_texts():
        new_text = text.text.translate(transtab).strip()
        if not new_text:
            print(text.id)


def main_join_sql():
    from glob import glob
    from os.path import basename, join
    sqls = []
    for file in glob("temp/*"):
        file_name = basename(file)
        with open(file) as f_in:
            sql = f_in.read()
            sqls.append(sql.replace("select text", "select text, '{}' as tipo, id ".format(file_name)))
    with open(join("temp", "union"), "w") as f_out:
        f_out.write(" union ".join(sqls))




if __name__ == "__main__":
    main_join_sql()
