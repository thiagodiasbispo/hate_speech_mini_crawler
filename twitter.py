# -*- coding: utf-8 -*-

import csv
from os.path import join, dirname
import json
from datetime import datetime, timedelta
import sys
import subprocess
from random import randint
from time import sleep
from util import get_conf_from_file, create_experimento_id
import tweepy
import csv
from db import DB


CURRENT_DIR = dirname(__file__)

twitter_conf = get_conf_from_file("files/twitter-conf.json",
                                    ("consumer_key","consumer_secret","access_token","access_token_secret")
                                 )

def do_auth():
    auth = tweepy.OAuthHandler(twitter_conf["consumer_key"], twitter_conf["consumer_secret"])
    auth.set_access_token(twitter_conf["access_token"], twitter_conf["access_token_secret"])
    return auth


def read_csv(file_name, delimiter = ","):
    with open(file_name) as f:
        for row in csv.reader(f, delimiter=delimiter):
            yield row


class HateSpeechListener(tweepy.StreamListener):
    def __init__(self, tipo, experimento_id, api):
        self.tipo = tipo
        self.experimento_id = experimento_id
        self.db = DB()
        super(HateSpeechListener, self).__init__(api)

    def output(self, status):
        print(self.tipo, status.timestamp_ms, status.text)
        created_at = datetime(1970, 1, 1) + timedelta(milliseconds=int(status.timestamp_ms))
        tweet = {"text": status.text,
                    "social_media": "t",
                    "social_media_object_id":status.id,
                    "hate_speech_type": self.tipo,
                    "experiment_id": self.experimento_id,
                    "social_user_details": json.dumps({"user_id":status.user.id}),
                    "create_date": created_at,
                    "published_date": created_at

                }
        self.db.save_comment(**tweet)

    def on_status(self, status):
        self.output(status)
        del status


def main(experiment_id):
    streams = []
    api = tweepy.API(auth_handler=do_auth())
    for row in read_csv("files/tags-twitter.csv"):
        tipo, tags = row[0], tuple([tag.strip() for tag in row[1].lower().split("/")])
        streams.append(tweepy.Stream(auth=api.auth, listener=HateSpeechListener(tipo, experiment_id, api)))
        streams[-1].filter(track=tags, languages=("br", "pt"), async=True)

    #  any(not s.running for s in streams) Será uma lista de "True" se todos os streams acabaram
    # Caso contrário, pelo menos um elemento da lista será "False", então a condição do while será igual ao valor de "not stop()"
    while not any(not s.running for s in streams):
        sleep(5)

def txt_to_postgres(experimento_id):
    db = DB()
    print (experimento_id)
    from datetime import timedelta, datetime
    hates = "Classe Social,Política,Xenofobia,Racismo,Homofobia,Idade_Geração,Misoginia,Deficiência,Aparência,Religioso"
    for hate in hates.split(","):
        tweets = []
        file_name = join(CURRENT_DIR, "twitter", hate + ".full.txt")
        with open(file_name) as f:
            for json_tweet in f:
                try:
                    t = json.loads(json_tweet)
                except Exception as e:
                    print(hate + ": " + str(e))
                    continue

                if t["retweeted"]:
                    continue

                created_at = datetime(1970, 1, 1) + timedelta(milliseconds=int(t["timestamp_ms"]))
                tweets.append({
                    "text": t["text"],
                    "social_media": "t",
                    "social_media_object_id":t["id"],
                    "hate_speech_type": hate,
                    "experiment_id": experimento_id,
                    "social_user_details": json.dumps({"user_id":t["user"]["id"] }),
                    "create_date": created_at,
                    "published_date": created_at
                })

        db.save_all_comments(tweets)
if __name__ == "__main__":
#    txt_to_postgres(randint(1, 1000000))
    experiment_id = create_experimento_id()
    while True:
        try:
            with open(str(experiment_id), "w") as _: pass
            main(experiment_id)
        except Exception as e:
            print(str(e))
            print("Sleeping by 10s")
            sleep(10)
