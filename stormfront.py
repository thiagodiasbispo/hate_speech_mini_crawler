from selenium import webdriver

from db import DB
from util import create_experimento_id


def get_next_page(driver):
    try:
        next_elem = driver.find_element_by_xpath("//a[contains(@title, 'Next Page')]")
        return next_elem.get_attribute("href")
    except:
        return None


def get_text_from_post_elem(post):
    try:
        quote = post.find_element_by_tag_name("div").text
        return post.text[len(quote):].strip()
    except:
        return post.text.strip()


def extract_posts(url):
    driver = webdriver.PhantomJS()
    while url:
        driver.get(url)
        posts = driver.find_elements_by_xpath("//div[contains(@id, 'post_message_')]")
        yield url, tuple(get_text_from_post_elem(post) for post in posts)
        url = get_next_page(driver)


def main():
    experiment_id = create_experimento_id()
    print("Experimento: ", experiment_id)
    db = DB()
    for url, posts in extract_posts("https://www.stormfront.org/forum/t733258-2/"):
        comments = [
                    {"text": post, "social_media":
                    "web_stormfront", "hate_speech_type":
                    "varios", "experiment_id": experiment_id}

                    for post in posts]
        db.save_all_web_page_comments(comments, url, "web_stormfront", experiment_id)
        print ("Done:", url)


if __name__ == "__main__":
    main()
