asn1crypto==0.22.0
bcrypt==3.1.3
beautifulsoup4==4.6.0
bs4==0.0.1
certifi==2017.4.17
cffi==1.10.0
chardet==3.0.4
cryptography==2.0.3
cssselect==1.0.1
cycler==0.10.0
Django==1.10
EasyProcess==0.2.3
facepy==1.0.9
google-api-python-client==1.6.2
httplib2==0.10.3
idna==2.5
lxml==3.8.0
matplotlib==2.0.2
numpy==1.13.1
oauth2client==4.1.2
oauthlib==2.0.2
paramiko==2.2.1
psycopg2==2.7.1
pyasn1==0.2.3
pyasn1-modules==0.0.9
pycparser==2.18
PyNaCl==1.1.2
pyparsing==2.2.0
python-dateutil==2.6.1
pytz==2017.2
requests==2.18.1
requests-oauthlib==0.8.0
rsa==3.4.2
scikit-learn==0.19.0
scipy==0.19.1
selenium==3.4.3
sip==4.19.3
six==1.10.0
sklearn==0.0
SQLAlchemy==1.1.11
sshtunnel==0.1.2
tweepy==3.5.0
uritemplate==3.0.0
uritools==2.0.0
urlextract==0.6
urllib3==1.21.1
wget==3.2
