import os
import csv
import json
from random import randint
from selenium.common.exceptions import TimeoutException

'''
   Lê o arquivo txt "file_name" retorna uma lista de todas as suas linhas
'''
def read_tag_like_txt_file(file_name):
    if not os.path.exists(file_name):
        print("Arquivo não existe: " + file_name)
        return tuple()

    with open(file_name) as f:
        for tag in f.read().split("\n"):
            if tag.strip():
                yield tag

'''
   Lê o arquivo csv "file_name" retorna uma lista de todas as suas linhas
'''
def read_tag_like_csv_file(file_name, delimiter = ","):
    if not os.path.exists(file_name):
        print("Arquivo não existe: " + file_name)
        return tuple()

    with open(file_name) as f:
        reader = csv.reader(f, delimiter=delimiter)
        for row in reader:
            yield row

'''
    Lê um arquivo de configuração presente em "file_name", converte para um dicionário python
    e verifica se ele possui todas as chaves presentes em "keys"
'''

def get_conf_from_file(file_name, keys=tuple()):
    if not os.path.exists(file_name):
        raise Exception("Arquivo '{}' não encontrado.".format(file_name))

    with open(file_name) as f:
        conf = json.load(f)

    for key in keys:
        if not key in conf:
            raise Exception("A chave '{}' não existe no arquivo '{}'".format(key, file_name))

    return conf

def try_load(driver, url_artigo):
    attempts = 0
    while True:
        try:
            driver.get(url_artigo)
        except TimeoutException as e:
            attempts+=1
            if attempts == 5:
                raise e
            print("TimeoutException, try again...")
            continue
        break

def press_load_more_button(driver, button_name):
    while True:
        try:
            button = driver.find_element_by_class_name(button_name)
            button.click()
        except Exception as e:
            break

def create_experimento_id():
    return randint(1, 1000000)
