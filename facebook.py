# -*- coding=utf-8 -*-

import csv
import glob
import os
from multiprocessing import Pool
import json
import facepy
import requests
import sys
import util
import db

from util import read_tag_like_txt_file, get_conf_from_file

"""
    Executa uma busca binária de status_id em lista

    :param: lista: Lista dos ids dos status que já foram recuperados
    :param: status_id: O id que deve ser pesquisados em lista
"""

def contem_status(lista, status_id):
    menor, maior = 0, len(lista) - 1
    while menor <= maior:
        mid = (menor + maior)//2
        if lista[mid] == status_id:
            return True
        elif status_id < lista[mid]:
            maior = mid - 1
        else:
            menor = mid + 1
    return False

"""
    Extrai os comentários da página

    :param page: página requerida
    :param extended_token: Token de vida útil longa do facebook
"""

def facebook(page, extended_token):

    print("Processing: {}".format(page))

    import json

    def status_id_to_int(status_id):
        return int(status_id.replace("_", ""))

    # recuperando a lista de status já processados da página por este script
    status_dones = list(status_id_to_int(l) for l in read_tag_like_txt_file("face/{}.status-done.txt".format(page)))
    status_dones.sort()

    def save_messages(status):
        is_new_file= not os.path.exists("face/{}.csv".format(page))
        lista_status_id = []
        with open("face/{}.csv".format(page), "a") as file_out:
            writer = csv.writer(file_out, delimiter=";", quoting=csv.QUOTE_ALL)
            if is_new_file: # Se não existe o arquivo, escreve o cabeçalho
                writer.writerow("status_id, status_text, comment_id, comment_text, comments, user_id, user_name, created_time".split(","))

            for s in status:
                if contem_status(status_dones, status_id_to_int(s["id"])): # Se o status fora processado, processa o próximo
                    continue

                lista_status_id.append("%s\n" % s["id"]) # buffer com os ids dos status processados
                comments = graph.get("{}/comments?&fields=id,message,created_time,comments,from".format(s["id"]))

                for comment in comments['data']:
                    inner_comments = json.dumps(comment["comments"]["data"]) if "comments" in comment else ''
                    message = s["message"] if "message" in s else ""
                    writer.writerow((s["id"], message, comment["id"],
                                     comment["message"], inner_comments,
                                     comment["from"]["id"], comment["from"]["name"], comment["created_time"]))

        #Salvando buffer de ids no arquivo
        with open("face/{}.status-done.txt".format(page), "a") as f:
            f.writelines(lista_status_id)

    graph = facepy.GraphAPI(extended_token, version=2.9)
    feed = graph.get(page + '/feed/', page=False) # Retorna um json de status paginado. As próxima lista de status está na url "feed["paging"]["next"]"
    save_messages(feed["data"])

    while True:
        try:
            data = feed["paging"]["next"]
            json_ = requests.get(data).json()
            save_messages(json_["data"])
        except KeyError as e: #Quando não houver mais status, a chave "next" não existirá em feed["paging"]
            break

    print("Done: {}".format(page))
    save_done(page) #Salvando a página concluída


def read_tags():
    return read_tag_like_txt_file("files/tags-facebook.txt")


def read_tads_done():
    for l in glob.glob("done/*"):
        yield os.path.basename(l)


def save_done(tag):
    with open("done/{}".format(tag), "w") as f:
        pass


def get_extended_access_token():
    face_conf = get_conf_from_file("files/facebook-conf.json", ("app_id","secret_key","token", "extended_token"))

    if not face_conf["extended_token"]:
        return facepy.get_extended_access_token(face_conf["token"], face_conf["app_id"], face_conf["secret_key"])

    return face_conf["extended_token"]


def to_data_base(experimento_id):
    print("Experimento: ", experimento_id)

    def get_comments(face_comment):
        # status_id, status_text, comment_id, comment_text, comments, user_id, user_name, created_time"
        user_details = {"user_id": face_comment[-3], "user_name": face_comment[-2]}
        more_information = json.dumps({"status_id": face_comment[0]})
        yield {
                "text": face_comment[1],
                "social_media": "f",
                "hate_speech_type": "varios",
                "experiment_id": experimento_id,
                "social_user_details": json.dumps(user_details),
                "social_media_object_id": face_comment[2],
                "more_information": more_information,
                "create_date": face_comment[-1],
                "published_date": face_comment[-1]
        }
        if not face_comment[4]:
            return

        for face_comment in json.loads(face_comment[4]):
            user_details = {"user_id": face_comment["from"]["id"], "user_name": face_comment["from"]["name"]}
            yield {
                "text": face_comment["message"],
                "social_media": "f",
                "hate_speech_type": "varios",
                "experiment_id": experimento_id,
                "social_user_details": json.dumps(user_details),
                "social_media_object_id": face_comment["id"],
                "more_information": more_information,
                "create_date": face_comment["created_time"],
                "published_date": face_comment["created_time"]
            }


    for file in glob.glob("face/*.csv"):
        content = util.read_tag_like_csv_file(file)
        next(content)
        for face_comment in content:
            comments = tuple(get_comments(face_comment))
            db.save_all_comments(comments)



def main():
    extended_token = get_extended_access_token()
    tags = set(read_tags()) - set(read_tads_done())

    with Pool(processes=3) as pool:
        retornos = []
        for index, tag in enumerate(tags):
            try:
                retornos.append(pool.apply_async(facebook, (tag, extended_token))) # Extrai os comentários de forma assíncrona
            except Exception as e:
                print ("Erro: " + str(e))

        for res in retornos:
            res.get()# Quando todos os workers tiverem terminado e retornar seus resultados (res.get()),
                     # o laço é encerrado e o script termina seu trabalho


if __name__ == "__main__":
    main()
