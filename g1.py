import json
import sys

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from os.path import dirname, join
import util
from db import DB

DEST_FOLDER = dirname(__file__)

def get_artigos_na_pagina(driver):
    artigos = driver.find_elements_by_class_name("feed-post-link")
    for elem_artigo in artigos:
        yield elem_artigo.get_attribute("href")


def extrair_links_topico_politica(output_file_name, primeira_pagina = 1, ultima_pagina = 1495):
    driver = webdriver.Firefox(timeout=10)

    with open(output_file_name, "a") as f:
        for pagina in range(primeira_pagina, ultima_pagina + 1):
            url_topico = "http://g1.globo.com/politica/index/feed/pagina-{}.html".format(pagina)
            print(url_topico)
            while True:
                try:
                    driver.set_page_load_timeout(5)
                    driver.get(url_topico)
                    for url_artigo in get_artigos_na_pagina(driver):
                        f.write(url_artigo + "\n")
                except TimeoutException as e:
                    print("TimeoutException, try again...")
                    continue
                break


def main_shell():
    import sys
    print (sys.argv)
    last, local_limit = int(sys.argv[1]), int(sys.argv[2])
    file_name = join(DEST_FOLDER, "web-content/g1-politica{}-{}.txt".format(last, local_limit))
    extrair_links_topico_politica(file_name, last, local_limit)

def get_create_date(comment_elem):
    data = comment_elem.find_element_by_class_name("glbComentarios-data")
    data = data.find_elements_by_tag_name("abbr")[0]
    return data.get_attribute("title")

def get_user_info(comment_elem):
    user_name = comment_elem.find_element_by_class_name("glbComentarios-dados-usuario-nome")
    return {"user_name": user_name.text}

def get_comment(comment_elem):
    return comment_elem.find_element_by_class_name("glbComentarios-texto-comentario").text

def extract_comment_from_artigo(experimento_id, url_artigo, driver_default = None):
    if not url_artigo:
        return {}

    driver = driver_default or webdriver.Firefox(timeout=10)

    util.try_load(driver, url_artigo)

    util.press_load_more_button(driver, "glbComentarios-botao-mais")

    try:
        comments = driver.find_elements_by_xpath("//li[starts-with(@id, 'comentario-')]")
    except Exception as e:
        return {}

    for comment_elem in comments:
        text = get_comment(comment_elem)
        if text:
            created_at = get_create_date(comment_elem)
            yield {
                "text": get_comment(comment_elem),
                "social_media": "web_g1",
                "hate_speech_type": "politica",
                "experiment_id": experimento_id,
                "social_user_details": json.dumps(get_user_info(comment_elem)),
                "create_date": created_at,
                "published_date": created_at
            }


def extract_comment_from_artigos(experimento_id, artigos):
    driver = webdriver.PhantomJS()
    i = 0
    db = DB()
    for artigo in artigos:
        if not artigo.strip():
            continue
        comments = tuple(extract_comment_from_artigo(experimento_id, artigo, driver))
        db.save_all_web_page_comments(comments, artigo, "web_g1", experimento_id)
        i+=1
        print("({}/{}) done: {}".format(i, len(artigos),artigo))
    driver.close()



def split(a, n):
    k, m = divmod(len(a), n)
    return tuple(a[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in range(n))


def split_file(n = 3):
    url_file_name = join(DEST_FOLDER, "web-content/g1/g1-politica-2.txt")
    url_dest_file_name = join(DEST_FOLDER, "web-content/g1/g1-politica-2.{}.txt")
    with open (url_file_name) as url_file:
        urls_list = split(url_file.readlines(), n)
        for i,urls in enumerate(urls_list):
            with open (url_dest_file_name.format(i+1),"w") as dest_file:
                dest_file.writelines(urls)


def main():
    db = DB()
    file_number = sys.argv[1]
    url_file = join(DEST_FOLDER, "web-content/g1/g1-politica-2.{}.txt".format(file_number))
    experimento_id = util.create_experimento_id()
    with open (url_file) as url_f:
        urls = set(url_f.read().split("\n")) - set(db.get_all_processed_pages_of_social_media("web_g1"))

    print("Novas urls: ", len(urls))
    print("Experimento: ", experimento_id)
    extract_comment_from_artigos(experimento_id, urls)
    #extract_comment_from_artigos(randint(1, 1000000), [urls], comments_file, done_file)


if __name__ == "__main__":
    main()
