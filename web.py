from time import sleep

import wget

# The BeautifulSoup module
from bs4 import BeautifulSoup

# The selenium module
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

import util

from itertools import chain

def r7_extract_url_from_topic(url, next_page_pattern, pages):
    for i in range(1, pages + 1):
        url = next_page_pattern.format(url=url, page=i)

"""
    Lista todas as urls de uma página de tópico da veja.abril.
"""


def veja_abril_extract_url_from_topic(url):

    def find_button_carregar_mais(driver):
        return driver.find_element_by_tag_name("button")

    def extrair_links(driver):

        elements = chain(driver.find_elements_by_class_name("list-item"),
                   driver.find_elements_by_class_name("not-loaded"),
                   driver.find_elements_by_class_name("list-item-title"))

        for elem in elements:
            link = elem.find_element_by_tag_name("a")
            yield link.get_attribute("href")

    def carregar_todos_os_itens(url):
        driver = webdriver.Chrome()
        driver.get(url)
        links = []
        while True:
            try:
                button = find_button_carregar_mais(driver)
                button.click()
                sleep(2)
                for link in extrair_links(driver):
                    links.append(link)
            except Exception as e:
                break
        return set(links)

    for i, link in enumerate(carregar_todos_os_itens(url)):
        print(i+1, link)

"""
    Recupera os comentários de uma página de notícia do uol.
"""
def uol(url):
    driver = webdriver.Firefox()
    driver.get(url) # load the web page
    button = driver.find_element_by_class_name("glbComentarios-botao-mais")

    while True:
        try:
            button.click()
            button = driver.find_element_by_class_name("glbComentarios-botao-mais")
        except:
            break

    for elem in driver.find_elements_by_class_name("glbComentarios-texto-comentario"):
        print (elem.text)

    driver.close()

if __name__ == "__main__":
    url = "http://veja.abril.com.br/noticias-sobre/homofobia/"
    veja_abril_extract_url_from_topic(url)

