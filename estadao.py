from selenium import webdriver
from os.path import dirname, join

DEST_FOLDER = dirname(dirname(__file__))

def get_artigos_na_pagina(driver):
    artigos = driver.find_elements_by_class_name("artigo")
    for elem_artigo in artigos:
        link = elem_artigo.find_element_by_tag_name("a")
        yield link.get_attribute("href")

'''
    É preciso informar a última página para que a função pegue todos os artigos disponíveis
'''
def extrair_links_topico(url_topico, output_file, primeira_pagina = 1, ultima_pagina = 186):
    print("Última página considerada: {}".format(ultima_pagina))
    driver = webdriver.Firefox()
    for pagina in range(primeira_pagina, ultima_pagina + 1):
        url_artigo = "{},{}".format(url_topico, pagina)
        driver.get(url_artigo)
        print(">>>> " + url_artigo)
        for url_artigo in get_artigos_na_pagina(driver):
            output_file.write(url_artigo)
            output_file.write("\n")


def main():
    with open(join(DEST_FOLDER, "web-content/estadao-xenofobia.txt"), "a") as f:
        extrair_links_topico("http://topicos.estadao.com.br/noticias-sobre-xenofobia", f, 1, 11)

if __name__ == "__main__":
    main()
