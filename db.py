# -*- coding: utf-8

from sqlalchemy import create_engine, Column, Integer, String, Date, Sequence, ForeignKey, or_, func, distinct
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.exc import IntegrityError
from itertools import chain
from sqlalchemy.ext.declarative import declarative_base
import psycopg2

engine = create_engine('postgres://postgres:postgres@localhost/hate_speech', echo=False)

Session = sessionmaker(bind=engine)

Base = declarative_base()

class HateSpeechType(Base):
    __tablename__ = "hate_speech_type"
    id = Column(Integer, Sequence('hate_speech_type_id_seq'), primary_key=True)
    name = Column(String)


class ProcessedUrls(Base):
    __tablename__ = "processed_urls"
    id = Column(Integer, Sequence('processed_urls_id_seq'), primary_key=True)
    url = Column(String, nullable=False, unique=True)
    social_media = Column(String, nullable=False)
    experiment_id = Column(Integer)

class CollectedTexts(Base):
    __tablename__ = "collected_texts"

    id = Column(Integer, Sequence('collected_texts_id_seq'), primary_key=True)
    search_type = Column(String)
    experiment_id = Column(Integer, nullable=False)
    search_string = Column(String)
    search_type = Column(String)
    social_media = Column(String, nullable=False)
    social_media_object_id = Column(String)
    social_user_details = Column(String)
    more_information = Column(String)
    text = Column(String, nullable=False)
    hate_speech_type = Column(String, nullable=False)
    published_date = Column(Date)
    create_date = Column(Date)
    url_id = Column(Integer, ForeignKey('processed_urls.id'), nullable=True)
    url = relationship("ProcessedUrls")



class DB(object):

    def __init__(self):
        self.session = Session()

    def add_hate_speech(self):
        hates = "Classe Social,Política,Xenofobia,Racismo,Homofobia,Idade_Geração,Misoginia,Deficiência,Aparência,Religioso"
        lista = (HateSpeechType(name = hate) for hate in hates.split(","))
        self.session.add_all(lista)
        self.session.commit()

    def get_all_collected_texts(self):
        return self.session.query(CollectedTexts).all()

    def delete(self, instance):
        self.session.delete(instance)
        self.session.commit()

    def get_all_processed_pages_of_social_media(self, social_media):
        tuple_pages = self.session.query(ProcessedUrls.url).\
        filter(ProcessedUrls.social_media == social_media).all()
        return tuple(page.replace("\n","") for page in chain(*tuple_pages))

    def get_all_collected_texts_that_match(self, tags):
        tags = tuple(tag.replace(" ","%") for tag in tags)
        filters = tuple(map(CollectedTexts.text.match, tags))
        return self.session.query(CollectedTexts).filter(or_(*filters)).all()

    def count_all_collected_texts_that_match(self, tags):
        tags = tuple(tag.replace(" ","%") for tag in tags)
        filters = tuple(map(CollectedTexts.text.match, tags))
        return self.session.query(func.count(distinct(CollectedTexts.text))).filter(or_(*filters)).scalar()


    def save_comment(self,**kwargs):
        self.save_all_comments((kwargs,))

    def save_all_web_page_comments(self, comments, url, social_media, experiment_id):
        processedUrl = ProcessedUrls(url=url, social_media=social_media, experiment_id = experiment_id)
        try:
            self.session.add(processedUrl)
            self.session.commit()
        except Exception as e:
            print(str(e))
            print("Pulando as url: ", url)
            return

        for comment in comments:
            comment["url_id"] = processedUrl.id
        self.save_all_comments(comments)

    def save_all_comments(self, comments):
        self.session.add_all((CollectedTexts(**args) for args in comments))
        self.session.commit()

if __name__ == "__main__":
    pass


#add_hate_speech()
#migrate_urls()
#Base.metadata.create_all(engine)

